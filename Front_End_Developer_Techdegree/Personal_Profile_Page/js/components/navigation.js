class MainNav extends HTMLElement {
    connectedCallback() {
        this.innerHTML = `
            <div class="main-nav">
                <ul class="nav">
                    <li class="name">
                        <span>Charlie Yu</span>
                    </li>
                    <li>
                      <a href="index.html">Home</a>
                    </li>
                    <li>
                        <a href="experience.html">Experience</a>
                    </li>
                </ul>
            </div>
        `;
    }
}

customElements.define('main-nav', MainNav); 