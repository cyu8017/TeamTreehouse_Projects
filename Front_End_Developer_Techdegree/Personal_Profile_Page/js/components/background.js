class MyBackgroundAndGoals extends HTMLElement {
    connectedCallback() {
        this.innerHTML = `
            <main class="flex">
                <div class="card">
                    <h2>My Background</h2>
                    <p>I’m an aspiring web designer and android developer who loves everything about technology. I've mostly lived in
                        the DC Metro Area and held many positions as a software developer. I’m excited to bring my life experience to
                        the process of building fantastic looking websites and creating awesome mobile apps.</p>
                    <p>I’ve been a Mobile Developer, Software Developer, UI/UX Designer/Developer, SharePoint Admin and SharePoint
                        Developer.</p>
                </div>
                <div class="card">
                    <h2>My Goals</h2>
                    <p>I want to master the process of building web sites and increase my knowledge, skills and abilities in:</p>
                    <ul class="skills">
                        <li>HTML</li>
                        <li>CSS</li>
                        <li>JavaScript</li>
                        <li>PHP</li>
                        <li>MySQL</li>
                    </ul>
                    <p>I’d like to work for a software company that focuses on web development.</p>
                </div>
            </main>
        `; 
    }
}

customElements.define('my-background-goals', MyBackgroundAndGoals); 