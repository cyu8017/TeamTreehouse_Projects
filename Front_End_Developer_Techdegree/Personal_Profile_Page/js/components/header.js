class HeaderSection extends HTMLElement {
    connectedCallback() {
        this.innerHTML = `
            <header>
                <img class="profile-image" src="images/bear.jpg" alt="Profile Image">
                <h1 class="tag name">Hello, I’m Charlie.</h1>
                <p class="tag location">My am located in Arlington, Virginia.</p>
            </header>
        `
    }
}

customElements.define('header-section', HeaderSection); 