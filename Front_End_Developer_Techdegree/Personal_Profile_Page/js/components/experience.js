class ExperienceSection extends HTMLElement {
    connectedCallback() {
        this.innerHTML = `
            <main>
                <div class="container">
                    <div class="row">
                        <center>
                            <div class="column">
                                <div class="cardexp">
                                    <h2>UX Developer</h2>
                                    <h3>August 2019 - Present</h3>
                                    <hr>
                                    <ul class="skills">
                                        <li>Adobe XD</li>
                                        <li>Adobe Illustrator</li>
                                        <li>HTML</li>
                                        <li>CSS</li>
                                        <li>JavaScript</li>
                                        <li>MySQL</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="column">
                                <div class="cardexp">
                                    <h2>Web Developer Specialist</h2>
                                    <h3>April 2019 - August 2019</h3>
                                    <hr>
                                    <ul class="skills">
                                        <li>Adobe Photoshop</li>
                                        <li>HTML</li>
                                        <li>CSS</li>
                                        <li>JavaScript</li>
                                        <li>MySQL</li>
                                        <li>PHP</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="column">
                                <div class="cardexp">
                                    <h2>Android Develpper</h2>
                                    <h3>January 2018 - March 2019</h3>
                                    <hr>
                                    <ul class="skills">
                                        <li>Android Studio</li>
                                        <li>Java</li>
                                        <li>Kotlin</li>
                                        <li>Firebase</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="column">
                                <div class="cardexp">
                                    <h2>Junior Software Develpper</h2>
                                    <h3>January 2016 - January 2017</h3>
                                    <hr>
                                    <ul class="skills">
                                        <li>HTML</li>
                                        <li>CSS</li>
                                        <li>JavaScript</li>
                                        <li>.NET</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </center>
                </div>
            </main>
        `;
    }
}

customElements.define('experience-section', ExperienceSection);