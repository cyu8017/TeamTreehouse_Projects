class FooterSection extends HTMLElement {
    connectedCallback() {
        this.innerHTML = `
            <footer>
                <ul>
                    <li>
                        <a href="https://www.linkedin.com/in/cyu8017/" target="_blank" class="social linkedin">LinkedIn</a>
                    </li>
                    <li>
                        <a href="https://github.com/cyu8017" target="_blank" class="social github">Github</a>
                    </li>
                </ul>
                <p class="copyright">Copyright 2019, Charlie Yu</p>
            </footer>
        `
    }
}

customElements.define('footer-section', FooterSection);