### Instructions 

If you've completed the online profile project, created a GitHub account and uploaded your project files to a GitHub repository, you;re ready to submit your project for review. BEfore you submt your project be sure to carefully check if your project meets the requirements needed to get a passing grade. 

Create or find an image file to represent yourself and a new image for the background. Be aware of the file size: keep the profile image under 400 KB and the background image under 1 MB. 

Using a text editor, modify the HTML of the index.html file 
- Add the images you collected in step 1 to the page. Please leave the class "profile-image" intact on your profile page. 
- Don't forget to update the text for your new profile image's alt attribute!

Personalize all the text on the page by changing the name, background and goals. Feel free to make up the information if you do not wish to share anything personal.

Add Your links to your GitHub, Twitter, and LinkedIn accounts. IF you do not have a Twitter or LinkedIn account, or do not want to share those, then you need to delete those links. 

Edit the href value of the Home link so it navigates to the index.html. Note: do not use root relative links, or links that begin with the leading forward slash /.

Create an additional page to expand on the online profile
- Create a second html file. Name the file experience.html or something similar such as resume.html. Change the href value of the Experience link in index.html to point to this new file. 
- Add HTML and text content to the additional page. Base your design and HTML on the index.html file or create your own HTML. 
- Edit the href value of the "Experience" link to navigate to your new page. 
- Feel free to change the name of the ("Interests, for example), just make sure you update the links to match the name of the page. 
- You can write custom CSS for the new pages, as long as you use style.css for both pages. 

Put your project files in a new GitHub repository on your GitHub account: 
- Make sure that you're only putting the files for this project in that repository.
    - Please do not create any new folders for this project. 
- The GitHub Desktop application can automatically create a new repository for you.

Make sure to check your code is valid by running it through an HTML and CSS validator: 
- There are a few exceptions that you don't need to fix: 
    - Don't worry about any warnings, we just need to check any errors that might be there. 
    - If CSS validator flats use of calc, vendor prefixes or pseudo-elements/pseudo-classes these errors should be ignored. 
    - If HTML validator flags use of pipe (|) in Google font links/URLs this error can also be ignored. 

### Overview 

For your first project, you'll practice web design skills by customizing a web page to create a personal profile. You'll modify the web page by adding graphics, custom text, and a stylish design. This is a great way for you to practice HTML and CSS skills. IT also gives you a web page you can use as a personal online calling card to advertise your experience, skills, and goals, and to provide links to your social media accounts on Twitter, LinkedIn, and GitHub. 

You'll also practice using GitHub, an important tool used by millions of developers to share code and work collaboratively on programming projects. Creating and using a GitHub account is also a great way to share your work with potential employers. 

### Note

The Front End Web Development Techdegree is meant to train you in HTML, CSS, and JavaScript on the client side. which means in the browser rather than on a server. So please avoid submitting any projects that rely on a server-side technology like PHP, Ruby on Rails, or that need to be supplied by a server in order to function.

A good practice is to check your project for cross browser compability. MAke sure it looks and functions correctly in multiple (at least three) browsers is an important part of being a top-notch developer. 
