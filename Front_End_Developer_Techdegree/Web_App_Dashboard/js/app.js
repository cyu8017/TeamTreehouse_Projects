$("document").ready(function () {
    $("#close-button").click(function () {
        $("#alert").fadeOut("slow");
    });

    $("#notification-bell").click(function () {
        if ($('#notifications').css("visibility") == "hidden") {
            $('#notifications').css("visibility", "visible").fadeIn();
        } else {
            $('#notifications').fadeToggle('slow', 'linear');
        }
        $("#alert-dot").fadeOut("slow");
        return false;
    });

    $(document).click(function () {
        $("notifications").fadeOutz("slow");
    });

    $('#notifications').click(function () {
        return false;
    });

    $('#send-button').click(function (e) {
        e.preventDefault();
        $('#message-form').hide();
        if ($('input#search').val().length === 0 || $('#textarea').val().length === 0) {
            $('#message-user').append('<div id="error"><p>Please include both a user name and a message. Fields cannot be empty.</p><p><button id="new-message">Try Again</button><p></div>');
            $('#new-message').click(function () {
                $('#confirmation').remove();
                $('#error').remove();
                $('#message-form').show();
            });
        } else if ($('input#search').val().length > 0) {
            $('#message-user').append('<div id="confirmation"><p>Thanks! Your message has been sent.</p><p><button id="new-message">New Message</button><p></div>');
            $('#new-message').click(function () {
                $('#confirmation').remove();
                $('#error').remove();
                $('#message-form')[0].reset();
                $('#message-form').show();
            });
        }
    });

    $('input[id="search"]').autoComplete({
        minChars: 1,
        source: function (term, suggest) {
            term = term.toLowerCase();
            let choices = ['Bucky Bibbler', 'Abraham Lincoln', 'Sheri Berry', 'Joan Pennington', 'Hillary Clinton', 'Erik Estrada', 'Jacob Jacobson', 'Weird Al Yankovich', 'Mickey Mouse', 'John Boy', 'Babe Ruth', 'Dolly Parton', 'Scout Finch'];
            let matches = [];
            for (i = 0; i < choices.length; i++) {
                if (~choices[i].toLowerCase().indexOf(term)) matches.push(choices[i]);
            }
            suggest(matches);
        },
    });

    $('#save-button').click(function (e) {
        e.preventDefault();
        let timezone = $('#timezone').val();
        localStorage.setItem('mytimezone', timezone);
        let email = $('#email-switch').prop('checked');
        localStorage.setItem('myemail', email);
        let public = $('#public-switch').prop('checked');
        localStorage.setItem('myprofile', public);
    });

    $('#timezone').val(localStorage.getItem('mytimezone'));

    if (localStorage.getItem('myemail') == "true") {
        $('#email-switch').prop('checked', true);
    } else {
        $('#email-switch').prop('checked', false);
    }

    if (localStorage.getItem('myprofile') == "true") {
        $('#public-switch').prop('checked', true);
    } else {
        $('#public-switch').prop('checked', false);
    }

    $('#cancel-button').click(function (e) {
        e.preventDefault();
        localStorage.clear();
        $('#timezone').val('a');
        $('#email-switch').prop('checked', false);
        $('#public-switch').prop('checked', false);
    });
});
