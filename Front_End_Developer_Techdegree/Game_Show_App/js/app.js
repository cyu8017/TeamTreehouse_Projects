let game;

const keyboardButtons = document.querySelectorAll('.key');
const scoreboard = document.querySelector('#scoreboard ol');
const newGameButton = document.querySelector('#btn__reset');
const overlay = document.querySelector('#overlay');

const newGame = () => {
    game = new Game();
    game.resetGameBoard();
    game.startGame();
}

newGameButton.addEventListener('click', () => {
    newGame();
});

keyboardButtons.forEach((button) => {
    button.addEventListener('click', (e) => {
        game.handleInteraction(e.target);
    })
});

window.addEventListener('keypress', (e) => {
    if (overlay.style.display === 'none') {
        keyboardButtons.forEach((button) => {
            if (button.textContent === e.key && !button.disabled) {
                game.handleInteraction(button)
            }
        });
    }
});

class Phrase {

    constructor(phrase) {
        this.phrase = phrase.toLowerCase();
    };

    addPhraseToDisplay() {
        const splitPhrase = this.phrase.split('');
        for (let i = 0; i < splitPhrase.length; i++) {
            let li = document.createElement('li');
            li.textContent = splitPhrase[i];
            splitPhrase[i] === ' ' ? li.className = 'hide space' : li.className = `hide letter ${splitPhrase[i]}`
            document.querySelector('#phrase ul').appendChild(li);
        }
    };

    checkLetter(letter) {
        return this.phrase.includes(letter);
    };

    showMatchedLetter(letter) {
        const matches = document.querySelectorAll(`.${letter}`);
        matches.forEach((match) => {
            match.classList.remove('hide');
            match.classList.add('show');
        });
    };

}

class Game {
    constructor() {
        this.missed = 0;
        this.phrases = this.createPhrases();
        this.activePhrase = null;
    };

    createPhrases() {
        return [
            new Phrase('One'),
            new Phrase('Two'),
            new Phrase('Three'),
            new Phrase('Four'),
            new Phrase('Five')
        ]
    };

    getRandomPhrase() {
        return this.phrases[Math.floor(Math.random() * this.phrases.length)];
    };

    startGame() {
        document.querySelector('#overlay').style.display = 'none';
        this.activePhrase = this.getRandomPhrase();
        this.activePhrase.addPhraseToDisplay();
    };

    checkForWin() {
        return document.querySelector('.hide.letter') === null;
    };

    removeLife() {
        const scoreboard = document.querySelector('#scoreboard ol');
        this.missed++;
        scoreboard.children[this.missed - 1].firstChild.src = 'images/lostHeart.png';
        if (this.missed === 5) {
            this.gameOver(false);
        }
    };

    gameOver(gameWon) {
        const message = document.querySelector('#game-over-message');
        if (gameWon) {
            message.textContent = 'Great Job!';
            overlay.className = 'win';
        } else {
            message.textContent = 'Sorry. Try Again!';
            overlay.className = 'lose';
        }
        newGameButton.textContent = 'Play Again?';
        overlay.style.display = '';
    };

    handleInteraction(button) {
        button.disabled = true;
        if (!this.activePhrase.checkLetter(button.textContent)) {
            button.classList.add('wrong');
            this.removeLife();
        } else {
            button.classList.add('chosen');
            this.activePhrase.showMatchedLetter(button.textContent);
            if (this.checkForWin()) {
                this.gameOver(true);
            }
        }
    };

    resetGameBoard() {
        document.querySelector('#phrase ul').innerHTML = '';
        keyboardButtons.forEach((button) => {
            button.disabled = false;
            button.classList.remove('chosen');
            button.classList.remove('wrong');
            button.classList.add('key');
        });
        for (let i = 0; i < scoreboard.children.length; i++) {
            scoreboard.children[i].firstChild.src = 'images/liveHeart.png';
        }
    };
}
