$('#search').keyup(function() {
    let $search = $(this).val(); 
    $("#gallery li").each(function() {
        let $altText = $(this).find("img").attr("alt"); 
        if ($altText.toLowerCase().search($search.toLowerCase()) > -1) {
            $(this).show(); 
        } else {
            $(this).fadeOut(500); 
        }
    });
}); 