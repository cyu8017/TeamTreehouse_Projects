// Dynamically Create Overlay HTML
const $overlay = $("<div id='overlay'></div>");
const $image = $("<img id='overlay-image'>");
const $caption = $("<p></p>");
const $exit = $('<div id="exit"><img src="icons/exit.png" alt="exit"></div>');
const $prevArrow = $('<div id="prevArrow"><img src="icons/left-arrow.png" alt="previous" /></div>');
const $nextArrow = $('<div id="nextArrow"><img src="icons/right-arrow.png" alt="next" /></div>');
const $index = 0;

// Function Update Image
function updateImage(imageLocation, imageCaption) {
    $image.attr("src", imageLocation);
    $caption.text(imageCaption);
}

// Function Next Image
function nextImage() {
    $index++;
    if ($index >= $("#gallery li").length) {
        $index = 0;
    }
    let nextImage = $("#gallery li").get($index).getElementsByTagName("a");
    let imageLocation = $(nextImage).attr("href");
    let imageCaption =  $(nextImage).children("img").attr("alt");
    
    // Update Overlay feature with ImageLocation and ImageCaption
    updateImage(imageLocation, imageCaption);
}

// Function Previous Image
function previousImage() {
    $index--;
    if ($index < 0) {
        $index = $("#gallery li").length - 1;
    }
    let prevImage = $("#gallery li").get($index).getElementsByTagName("a");
    let imageLocation = $(prevImage).attr("href");
    let imageCaption =  $(prevImage).children("img").attr("alt");

    // Update Overlay feature with ImageLocation and ImageCaption
    updateImage(imageLocation, imageCaption);
}

// Append overlay feature over body.
$("body").append($overlay);

// Gallery Image Action Listener
$("#gallery a").click(function(event) {
    event.preventDefault();
    let imageLocation = $(this).attr("href");
    let imageCaption =  $(this).children("img").attr("alt");
    $overlay.append($exit);
    updateImage(imageLocation, imageCaption);
    $overlay.append($image);
    $index = $(this).parent().index();
    $overlay.append($caption);
    $image.after($prevArrow);
    $image.before($nextArrow);
    $overlay.fadeIn(1500);
});

// Next Arrow Action Listener
$nextArrow.on("click", function(event) {
    nextImage();
});

// Previous Arrow Action Listener
$prevArrow.on("click", function(event){
    previousImage();
});

// Right Arrow Key Pressed Listener
$("body").keydown(function(event){
    if ( event.which == 39 ) {
        nextImage();
    }
});

// Left Arrow Key Pressed Listener
$("body").keydown(function(event){
    if ( event.which == 37 ) {
        previousImage();
    }
});

// Exit Button Event Listener
$exit.on("click", function() {
    $overlay.fadeOut(1000).hide();
});

// Escape Key Event Listener 
$("body").keydown(function(event) {
    if (event.which == 27) {
        $overlay.fadeOut(1000).hide();
    }
});
