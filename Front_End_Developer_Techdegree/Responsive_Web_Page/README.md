### Instructions:

In this project, you'll build a responsive, mobile-first layout using HTML and CSS. The layout should demonstrate an understanding of responsive design by adjusting to accomodate small, medium, and large screen sizes. 

You will write CSS to style the page for a small mobile device first. Then, using min-width media queries, you'll add breakpoints to adjust the layout for wider tablet and desktop screens. 

To complete this project, follow the instructions below: 
- Create an index.html and a styles.css file. 
- Create a folder called css and put your styles.css file inside it. The name of the folder should not be capitalized. 
- Link the styles.css file to index.html. 

### Note: 

THe Front End Web Development Techdegree is meant to train you in HTML, CSS< and JavaSCript, and let you practice and show your mastery of these fundamental building blocks of the web. Because of that, please avoid using frameworks like Bootstrap, Foundation, Skeleton, and so on for this project. EVen though you may end up using frameworks like thse professionally, you still need to kow and be able to implement designs with your own knowledge of HTML, CSS, and JavaScript.

A good practice is to check your project for cross browser compatibility.

In addition, please avoid submitting any projects that rely on server-side technology like PHP or Ruby on Rails. 

### Build the layout using a mobile first design: 

- Make sure the HTML file includes the viewport meta tag in the head of the document. 
- Look at the provided mockup for the mobile device and add the same header, titles, content and footer information into your index.html file. 
- Use the provided images for the portfolio gallery images shown in the mockups. 
- Use a font from Google Fonts for the text. 
- Use CSS to style your layout to match the provided mobile mockup. Make sure your mobile desin matches the mockup at 320px screen size. 

### Once you have everything in place for the mobile layout, use media queries to add breakpoints to adjust the layout for wider tablet and desktop screens: 

- The design does not need to be exact, but the general spacing and arrangement of the elements should match the design of the mockups for mobile, tablet and desktop. 
- Feel free to replace the profile image and customize the text, but the layouts should match the mockups. 

### Make sure to check you code is valid by running it through an HTML and CSS validator:

- Links to the validators can be found in the Project Resources. THis will help you spot any errors that might be in your code.
- There are a few exceptions that you don't need to fix: 
    - Don't worry about any warnings, we just need you to check any errors that might be there. 
    - If CSS validator flats use of calc, vendor prefixes or pseudo-elements/pseudo-classes these errors should be ignored. 
    - If HTML validator flags use of pipe ('|') in Google font-links/URLs this error can also be ignored. 
