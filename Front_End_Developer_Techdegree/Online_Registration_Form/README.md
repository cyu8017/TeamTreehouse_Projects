### Treehouse Project - An Online Registration Form

In this project, you'll buils a responsive, mobile-friendly registration form using a wide variety of HTML form input types and attributes. Using the supplied mockup files, you'll build a mobile and desktop version of the form using media queries, and a "mobile-first" approach.

### Note
The Front End Web Development Techdegree is meant to train you in HTML, CSS, and JavaScript, and let you practice and show your mastery of these fundamentals building blocks of the web. Because of that, please avoid using frameworks like Bootstrap, Foundation, Skeleton, and so on for this project. Even though you may end up using frameworks like these professionally, you still need to know and be able to implement designs with your own knowledge of HTML, CSS, and JavaScript. 

In addition, please avoid submitting and projects that rely on a server-side technology like PHP or Ruby on Rails.
