let employees = [];

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function addEmployeesToDirectory(employees) {
    let galleryHTML = '<ul class="directory">';
    $.each(employees, function (i, employee) {
        let firstName = capitalize(employee.name.first);
        let lastName = capitalize(employee.name.last);
        let city = capitalize(employee.location.city);
        galleryHTML += '<li id="' + i + '">';
        galleryHTML += '<a><img src="' + employee.picture.large + '"></a>';
        galleryHTML += '<p><strong>' + firstName + ' ' + lastName + '</strong><br>';
        galleryHTML += employee.email + '<br>';
        galleryHTML += city + '</p></li>';
    });
    galleryHTML += '</ul>';
    $('.employee-directory').html(galleryHTML);
}

function employeeClickEventListener() {
    $('li').click(function () {
        let id = $(this).attr('id');
        let idNumber = parseInt(id, 10);
        displayEmployeeModal(idNumber);
    });
}

function displayEmployeeModal(index) {
    let employee = employees[index];
    let firstName = capitalize(employee.name.first);
    let lastName = capitalize(employee.name.last);
    let address = formatAddress(employee);
    let dob = formatDateOfBirth(employee.dob);
    let modalContent = '<div class="modal-content">';
    modalContent += '<span class="close">&times;</span>';
    modalContent += '<img src="' + employee.picture.large + '">';
    modalContent += '<p><strong>' + firstName + ' ' + lastName + '</strong><br>';
    modalContent += '<br>' + employee.login.username + '<br>' + employee.email + '<br>';
    modalContent += '<br><hr><br>' + employee.cell + '<br><br>';
    modalContent += address + '<br>';
    modalContent += 'Birthday: ' + dob + '</p>';
    modalContent += '<span class="buttons">';
    modalContent += '<button class="back">Back</button>';
    modalContent += '<button class="next">Next</button></span>';
    modalContent += '</div>';
    $('#employee-modal').append(modalContent);
    $('.modal').css('display', 'block');
    addEventListenersToModal(index);
}

function formatDateOfBirth(string) {
    let date = new Date(Date.parse(string));
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let year = date.getYear();
    let dob = month + '/' + day + '/' + year;
    return dob;
}

function formatAddress(employee) {
    let city = capitalize(employee.location.city);
    let state = capitalize(employee.location.state);
    let address = employee.location.street + '<br>'
    address += city + ', ' + state;
    address += ' ' + employee.location.postcode + ', ';
    address += employee.nat + '<br>';
    return address;
}

function addEventListenersToModal(idNumber) {
    $('.close').click(function () {
        $('.modal').css('display', 'none');
        $('.modal-content').remove();
    });

    $('.back').click(function () {
        let last = idNumber - 1;
        if (idNumber > 0) {
            $('.modal-content').remove();
            displayEmployeeModal(last);
        }
    });

    $('.next').click(function () {
        let next = idNumber + 1;
        if (idNumber < 11) {
            $('.modal-content').remove();
            displayEmployeeModal(next);
        }
    });
}

function searchEmployees(input) {
    let searchTerm = input.toLowerCase();
    let $employees = $('p:contains(' + searchTerm + ')').closest('li');
    $('li').hide();
    $employees.show();
}

$('#search').keyup(function () {
    let searchTerm = $('#search').val();
    searchEmployees(searchTerm);
});

$.ajax({
    url: 'https://randomuser.me/api/?results=12&nat=us',
    dataType: 'json',
    success: function (data) {
        employees = data.results;
        addEmployeesToDirectory(employees);
        employeeClickEventListener();
    }
});